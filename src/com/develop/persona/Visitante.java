package com.develop.persona;

import java.util.Scanner;

public class Visitante {
    String estadoOrigen;
    String tipoAcceso;
    String tipoDescuento;
    int noPersonas;
    
    Scanner sca = new Scanner(System.in);
    
    public void crearVisitante(){
        
        System.out.println("Formulario para Visitante");
        Persona perVis = new Persona();
        perVis.crearPersona();
        System.out.println("Escribe tu estado de origen");
        estadoOrigen = sca.nextLine();
        System.out.println("Escribe tu tipo de acceso");
        tipoAcceso = sca.nextLine();
        System.out.println("Escribe tu tipo de descuento");
        tipoDescuento = sca.nextLine();
        System.out.println("Escribe cuantas personas te acompanian");
        noPersonas = sca.nextInt();
        
        
    }
    
    public void muestraTrabajador(Persona perVis){
        System.out.println("Tu nombre es: " + perVis.nombre 
                + "\nApellido paterno: " + perVis.apPaterno 
                + "\nApellido materno: " + perVis.apMaterno
                + "\nEdad: " + perVis.edad
                + "\nGenero: " + perVis.genero
                + "\nEstado de origen: " + estadoOrigen
                + "\nTipo de acceso: " + tipoAcceso
                + "\nTipo de descuento: " + tipoDescuento
                + "\nNomero de personas: $" + noPersonas);
    }
}
