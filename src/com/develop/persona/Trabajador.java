package com.develop.persona;

import java.util.Scanner;

public class Trabajador {
    String puesto;
    String curp;
    String nss;
    float sueldo;
    
    Scanner sca = new Scanner(System.in);
    
    public void crearTrabajador(){
        
        System.out.println("Formulario para trabajador");
        Persona perTrab = new Persona();
        perTrab.crearPersona();
        System.out.println("Escribe tu puesto");
        puesto = sca.nextLine();
        System.out.println("Escribe tu curp");
        curp = sca.nextLine();
        System.out.println("Escribe tu nss");
        nss = sca.nextLine();
        System.out.println("Escribe tu sueldo");
        sueldo = sca.nextFloat(); 
        muestraTrabajador(perTrab);
        
    }
    
    public void muestraTrabajador(Persona perTrab){
        System.out.println("Tu nombre es: " + perTrab.nombre 
                + "\nApellido paterno: " + perTrab.apPaterno 
                + "\nApellido materno: " + perTrab.apMaterno
                + "\nEdad: " + perTrab.edad
                + "\nGenero: " + perTrab.genero
                + "\nPuesto: " + puesto
                + "\nCURP: " + curp
                + "\nNSS: " + nss
                + "\nSueldo: $" + sueldo);
    }
}
