package com.develop.animal;

import java.util.Scanner;

public class Animal {

    String nombre;
    String especie;
    String tamanio;
    float peso;
    String alimentacion;

    Scanner sca = new Scanner(System.in);

    public void agregarAnimal() {
        int cantidad;
        System.out.println("Cuantos animales quieres crear?");
        cantidad = sca.nextInt();

        String arregloAnimales[] = new String[cantidad];

        for (int i = 0; i < arregloAnimales.length; i++) {
            System.out.println("Escribe el nombre del animal: ");
            nombre = sca.next();
            System.out.println("Escribe la especie del animal: ");
            especie = sca.next();
            System.out.println("Escribe el tamaño del animal: "
                    + "\nChico, Mediano, Grande, Jumbo");
            tamanio = sca.next();
            System.out.println("Escribe el peso del animal: ");
            peso = sca.nextFloat();
            System.out.println("Escribe el tipo de alimentación del animal: ");
            alimentacion = sca.next();
            arregloAnimales[i] = nombre;
        }

        System.out.println("\nNombre de los animales: \n");
        for (String animal : arregloAnimales) {
            System.out.println(animal);
        }
    }
/ /**//
}
