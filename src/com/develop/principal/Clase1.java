package com.develop.principal;

public class Clase1 {
    public static void main(String[] args) throws Throwable{
        Clase2 clase = new Clase2();
        clase.hola();
        clase.finalize();
        int x = 3;
        int y = ++x*5/x-- + --x;
        System.out.println(x);
        System.out.println(y);
        //System.out.println(((Object)(/*Variable*/)).getClass().getSimpleName());
        
    }
}

class Clase2{
    public void hola(){
        System.out.println("Hola");
        
    }
    @Override
    protected void finalize() throws Throwable{
        super.finalize();
    }
}