package com.develop.principal;

import com.develop.animal.Animal;
import com.develop.persona.Trabajador;
import com.develop.persona.Visitante;
import java.util.Scanner;

public class Principal {
    public static void main(String []args){
        /*Trabajador trab = new Trabajador();
        trab.crearTrabajador();*/
        solicitaMenu();
        
    }
    
    public static void menu(int opcion){ 
        if(opcion == 1){
            Animal animal = new Animal();
            animal.agregarAnimal();
        }else if(opcion == 2){
            Trabajador trab = new Trabajador();
            trab.crearTrabajador();
        }else if(opcion == 3){
            Visitante visitante = new Visitante();
            visitante.crearVisitante();
        }else if(opcion == 4){
            System.out.println("Secciones");
        }else{
            System.out.println("Opcion no valida.");
            solicitaMenu();
        }
    }
    
    public static void solicitaMenu(){
        Scanner sc = new Scanner(System.in);
        int opcion;
        System.out.println("Que deseas hacer? \n1)Crear animal \n2)Crear empleado \n3)Crear visitante \n4)Ver secciones");
        opcion = sc.nextInt();
        opcion = ((opcion >= 1) && (opcion <= 4))? opcion : 0;
        menu(opcion);
    }
}
